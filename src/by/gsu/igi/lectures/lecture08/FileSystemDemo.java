package by.gsu.igi.lectures.lecture08;

import java.io.*;
import java.util.Arrays;

/**
 * @author Evgeniy Myslovets
 */
public class FileSystemDemo {

    public static void main(String[] args) throws IOException {
        File currentDirectory = new File(".");
        File file = new File(currentDirectory, "README.md");
        File inexist = new File(currentDirectory, "foo.bar");

        System.out.println(file.exists());
        System.out.println(inexist.exists());
        System.out.println(file.getPath());
        System.out.println(file.getAbsolutePath());
        System.out.println(inexist.getAbsolutePath());
        System.out.println(file.getCanonicalPath());

        File tempFile = File.createTempFile("foo", ".bar");
        System.out.println(tempFile.getAbsolutePath());
        tempFile.deleteOnExit();

        RandomAccessFile fileAccess = new RandomAccessFile(file, "r");
        String firstLine = fileAccess.readLine();
        System.out.println(firstLine);
        fileAccess.seek(100);
        System.out.println(fileAccess.read());

        System.out.println("Files in current dir:");
        for (File f : currentDirectory.listFiles()) {
            System.out.println((f.isDirectory() ? "DIR:" : "FILE:") + f.getName());
        }
    }
}