package by.gsu.igi.students.AleksandraSemenova.lab5;

/**
 * Created by Aleksandra Semenova.
 */
public interface Speakable {
    void speak();
}
