package by.gsu.igi.students.DruzenkoMax.lab5;

/**
 * Created by Druzenko Max.
 */
public interface Speakable {
    void speak();
}
