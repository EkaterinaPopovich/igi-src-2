package by.gsu.igi.students.EkaterinaPopovich.lab3;

import java.util.Scanner;

/**
 * Created by Ekaterina Popovich.
 */
public class ArrayOperationsDemo {

    private static int findMax(int[] numbers) {
        int max = numbers[0];

        for (int number : numbers) {
            if (number > max) {
                max = number;
            }
        }

        return max;
    }

    private static int findMin(int[] numbers) {
        int min = numbers[0];

        for (int number : numbers) {
            if (number < min) {
                min = number;
            }
        }

        return min;
    }

    private static double calculateAverage(int[] numbers) {
        int average = 0;

        for (int number : numbers) {
            average += number;

        }
        return average / (double) numbers.length;
    }

    private static int product(int[] numbers) {
        int product = 1;

        for (int number : numbers) {
            product *= number;
        }

        return product;
    }

    private static int sum(int[] numbers) {
        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }

        return sum;
    }

    private static int difference(int[] numbers) {
        int diff = 0;

        for (int number : numbers) {
            diff -= number;
        }

        return diff;
    }

    public static void main(String[] args) {
        // читаем из консоли последовательность целых чисел,
        int[] numbers = readArray();

        // вызываем вычислительные методы для полученного массива
        processArray(numbers);
    }

    private static int[] readArray() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество элементов:");
        int size = scanner.nextInt();

        int[] numbers = new int[size];
        for (int i = 0; i < size; i++) {
            System.out.println("Введите " + (i + 1) + "-й элемент:");
            numbers[i] = scanner.nextInt();
        }

        return numbers;
    }

    private static void processArray(int[] numbers) {
        int max = findMax(numbers);
        System.out.println("Максимальный элемент: " + max);

        int min = findMin(numbers);
        System.out.println("Минимальный элемент: " + min);

        double average = calculateAverage(numbers);
        System.out.println("Среднее значение: " + average);

        int product = product(numbers);
        System.out.println("Произведение элементов: " + product);

        int sum = sum(numbers);
        System.out.println("Сумма элементов: " + sum);

        int difference = difference(numbers);
        System.out.println("Разность элементов: " + difference);
    }
}
